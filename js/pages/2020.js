"use strict"

const Beez = (function() {
	const data = {

	};

	const events = {

	};

	const methods = {
	  


	};

	const initialize = function() {
	 

	};

	return {
	  	init: initialize
	};
})();


document.addEventListener(
	'DOMContentLoaded',
	function() {
		Beez.init();
		
       
	},
	false
);

window.addEventListener('load', () => {
	$('.owl-carousel').owlCarousel({
		stagePadding: 50,
		dots: false,
		loop:true,
		margin:10,
		nav:false,
		navText: ["<img src='img/flecha-hacia-arriba-1.svg'>","<img src='img/flecha-hacia-arriba-2.svg'>"],
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	}); 
});



function despliegue(id) {
	setTimeout(() => {
		document.getElementById(id).scrollIntoView({
			behavior: "smooth",
			block: "center",
			inline: "nearest"
		});
	}, 500);
}

